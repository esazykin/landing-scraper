<?php

echo "Test client\n";

$conf = (require_once __DIR__ . "/config/config.php")['gearman'];

$gmclient = new GearmanClient();
$gmclient->addServer();

do {
    $result = $gmclient->doNormal(
        $conf['functions']['scrape'],
        serialize([
            'params' => [
                "task_domain_id" => 1,
                "in_domain" => "http://tresh.fresh5.com.ua/",
                "out_domain" => "http://tresh-fresh.ru",
                "isp_id" => 1
            ]
        ])
    );

    switch ($gmclient->returnCode()) {
        case GEARMAN_WORK_DATA:
            echo "Data: $result\n";
            break;
        case GEARMAN_WORK_STATUS:
            list($numerator, $denominator) = $gmclient->doStatus();
            echo "Status: $numerator/$denominator complete\n";
            break;
        case GEARMAN_WORK_FAIL:
            echo "Failed\n";
            exit;
        case GEARMAN_SUCCESS:
            echo "Success: $result\n";
            break;
        default:
            echo "RET: " . $gmclient->returnCode() . "\n";
            exit;
    }
} while ($gmclient->returnCode() != GEARMAN_SUCCESS);
