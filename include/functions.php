<?php

use Illuminate\Database\Capsule\Manager as Capsule;

function iniErrorHandler()
{
    set_error_handler(function ($errno, $errstr, $errfile, $errline) {
        if ((error_reporting() & $errno)) {
            $msg = "file: $errfile, line: $errline, errno: $errno, message: $errstr" . PHP_EOL;
            error_log($msg, 3, __DIR__ . "/../storage/error-" . date('Y-m-d') . ".log");
        }
        return true;
    });
}

function config($key)
{
    static $config;
    $config = $config ?? require_once __DIR__ . '/../config/config.php';
    return data_get($config, $key);
}

function successResult($msg)
{
    exit(json_encode(['success' => ['msd' => $msg, 'code' => 0]]));
}

function errorResult($msg, $code)
{
    exit(json_encode(['error' => ['msd' => $msg, 'code' => $code]]));
}

function getFileContent(string $url, array $body = []) : string
{
    static $client;
    $client = $client ?? new \GuzzleHttp\Client();
    try {
        if (empty($body)) {
            $res = $client->get($url);
        } else {
            $res = $client->post($url, ['form_params' => $body]);
        }
        $body = $res->getBody()->getContents();
    } catch (\Exception $e) {
        errorResult($e->getMessage(), 30);
        $body = '';
    }
    return $body;
}

function db()
{
    static $capsule;
    if (!$capsule) {
        $capsule = new Capsule;
        $capsule->addConnection([
            'driver' => 'mysql',
            'host' => config('db.host'),
            'database' => config('db.database'),
            'username' => config('db.user'),
            'password' => config('db.password'),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
        ]);
        $capsule->setAsGlobal();
    }
    return $capsule;
}