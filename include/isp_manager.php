<?php

use Illuminate\Support\Collection;
use Spyric\ISPManager\Manager;

function ISPManager(array $config) : stdClass
{
    static $ISPManager, $result;
    if (!$ISPManager) {
        $ISPManager = new Manager(...array_values($config));
        $result = new stdClass();

        $result->createDomain = function ($name, $email) use ($ISPManager) : array {
            $domain = $ISPManager->WWWDomain();
            $domain->name = $name;
            $domain->email = $email;
            try {
                return $domain->save();
            } catch (Exception $e) {
                return ['error' => $e->getMessage()];
            }
        };

        $result->getDomains = function () use ($ISPManager) : Collection {
            $domain = $ISPManager->WWWDomain();
            try {
                return $domain->all();
            } catch (Exception $e) {
                return collect();
            }
        };
    }
    return $result;
}
