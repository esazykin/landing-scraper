<?php

function processFile($scrapHostname, $url, $contentType, $hostname, $resourceNames) : string
{
    for ($i = 0; $i < 5; $i++) {
        $content = getFileContent($url);
        if (!empty($content)) {
            break;
        }
    }
    if (empty($content)) {
        return '';
    }

    if (strpos($contentType, 'text') !== false || strpos($contentType, 'javascript') !== false) {
        $content = preg_replace("#(www\.)?$scrapHostname#imu", $hostname, $content);

        $curAbsPath = explode('/', parse_url($url)['path']);
        array_pop($curAbsPath);
        $curAbsPath = implode('/', $curAbsPath) . "/";

        foreach ($resourceNames as $path => $name) {
            $quoted = preg_quote($name);
            $content = preg_replace_callback(
                "/(?:[\:_\-\+a-z0-9\.\/])*$quoted/im",
                function ($matches) use ($curAbsPath, $hostname, $path) {
                    $resPath = "/" . getAbsolutePath($curAbsPath . $matches[0]);
                    if ($resPath == $path) {
                        return "http://$hostname" . $resPath;
                    }
                    return $matches[0];
                },
                $content
            );
        }
    }

    if (strpos($contentType, 'html')) {
        $foreign = [
            'google-analytics' => function ($content) {
                $content = preg_replace('/_gaq\.push\(\[[\'"]_setAccount[\'"],\s*[\'"](.*)[\'"]\]\);/', "_gaq.push(['_setAccount', '']);", $content);
                return preg_replace("/ga\\('create', '(.*)', 'auto'\\);/i", "ga('create', '0', 'auto');", $content);
            },
            'mc.yandex' => function ($content) {
                $content = preg_replace('@(mc\.yandex\.ru/watch)/\d+@i', '$1/0', $content);
                return preg_replace(
                    '/w\.yaCounter(\d*)\s*=\s*new\s+Ya.Metrika\(\{\s*id\s*:\s*[\'"]?\d+[\'"]?/im',
                    'w.yaCounter$1 = new Ya.Metrika({id:0',
                    $content
                );
            },
            'code.jivosite' => function ($content) {
                return preg_replace(
                    '@[\'"]//code\.jivosite\.com/script/widget/[\'"]\s*\+\s*[a-z_]+@im',
                    '"//code.jivosite.com/script/widget/0"',
                    $content
                );
            },
            'siteheart' => function ($content) {
                return preg_replace(
                    '@[\'"]widget\.siteheart\.com/widget/sh/[\'"]\s*\+\s*[a-z_]+@im',
                    '"widget.siteheart.com/widget/sh/0"',
                    $content
                );
            },
            'liveinternet' => function ($content) {
                return preg_replace('@counter\.yadro@i', '', $content);
            },
            'openstat.net' => function ($content) {
                return preg_replace('@openstat\.net@i', '', $content);
            },
            'hotlog.ru' => function ($content) {
                $content = preg_replace('@js\.hotlog\.ru/dcounter/[0-9a-z]+\.js@i', 'js.hotlog.ru/dcounter/0.js', $content);
                $content = preg_replace('@click.hotlog.ru/\?[0-9a-z]+@i', 'js.hotlog.ru/dcounter/0.js', $content);
                return preg_replace('@hotlog.ru/cgi-bin/hotlog/count\?s=[0-9a-z]+@i', 'js.hotlog.ru/dcounter/0.js', $content);
            },
            'googletagmanager' => function ($content) {
                return preg_replace('@googletagmanager\.com/gtm\.js\?id=@i', 'googletagmanager.com/gtm.js?id=FAKE_ID_', $content);
            },
        ];
        foreach ($foreign as $k => $v) {
            $content = $v($content);
        }
//        $content = preg_replace(
//            '/<head>/im',
//            "<head><script id='injected-script-ajax-proxy' src='//$hostname/" . md5($hostname) . "_ajax_proxy.js'></script>",
//            $content
//        );
//        $content = preg_replace(
//            '/<\/\s*body\s*>/im',
//            "<script id='injected-script-form-proxy' src='//$hostname/" . md5($hostname) . "_form_proxy.js'></script></body>",
//            $content
//        );
    }
    return $content;
}

function getAbsolutePath($path)
{
    $path = str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $path);
    $parts = array_filter(explode(DIRECTORY_SEPARATOR, $path), 'strlen');
    $absolutes = [];
    foreach ($parts as $part) {
        if ('.' == $part) {
            continue;
        }
        if ('..' == $part) {
            array_pop($absolutes);
        } else {
            $absolutes[] = $part;
        }
    }
    return implode(DIRECTORY_SEPARATOR, $absolutes);
}