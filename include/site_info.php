<?php

function getSiteInfo(string $address) : array
{
    $cmd = escapeshellcmd("phantomjs tool/scraper.js $address");
    $descriptorspec = [['pipe', 'r'], ['pipe', 'w'], ['pipe', 'w']];
    $pipes = [];
    $process = proc_open($cmd, $descriptorspec, $pipes, null, null);
    if (!is_resource($process)) {
        errorResult('proc_open() did not return a resource', 3);
    }

    $result = stream_get_contents($pipes[1]);
    $result = json_decode($result, true);
    $log = stream_get_contents($pipes[2]);

    fclose($pipes[0]);
    fclose($pipes[1]);
    fclose($pipes[2]);

    if (proc_close($process) !== 0) {
        errorResult('proc_close() did not return a success code', 4);
    }

    return $result;
}