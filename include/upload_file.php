<?php

use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Ftp as FtpAdapter;

function uploadFiles(string $scrapHostname, string $hostname, array $siteInfo, $isp, $tasksDomainId)
{
    $exportableData = [
        'user_id' => 1,
        'project' => [
            'hostname' => $hostname,
            'dir_root' => "/www/$hostname",
            'name' => $hostname,
            'screen' => "screen_" . time() . ".jpeg",
            'isp_id' => $isp->id,
            'tasks_domain_id' => $tasksDomainId,
        ],
        'html' => [],
    ];
    $adapter = new FtpAdapter([
        'host' => $isp->ip_address,
        'username' => $isp->login,
        'password' => $isp->password,
        /** optional config settings */
        'port' => 21,
        'root' => "/www/$hostname",
        'passive' => true,
//        'ssl' => true,
        'timeout' => 300,
    ]);
//    $adapter = new League\Flysystem\Adapter\Local(__DIR__ . '/../storage');
    $filesystem = new Filesystem($adapter);
//    $filesystem->createDir($hostname);
//    $filesystem->put("$hostname/screen.jpeg", base64_decode($siteInfo['screen']));
//    $filesystem->put("$hostname/" . md5($hostname) . "_ajax_proxy.js", file_get_contents('./inject/ajax_proxy.js'));
//    $filesystem->put("$hostname/" . md5($hostname) . "_form_proxy.js", file_get_contents('./inject/form_proxy.js'));
//    $filesystem->put("$hostname/catch-all-request.php", file_get_contents('./inject/catch-all-request.php'));
    $r = $filesystem->put($exportableData['project']['screen'], base64_decode($siteInfo['screen']));
    $r = $filesystem->put(md5($hostname) . "_ajax_proxy.js", file_get_contents('./inject/ajax_proxy.js'));
    $r = $filesystem->put(md5($hostname) . "_form_proxy.js", file_get_contents('./inject/form_proxy.js'));

    $c = file_get_contents('./inject/catch-all-request.php');
    $c = str_replace('$taskId = 0; //!', '$taskId = ' . "$tasksDomainId;", $c);
    $c = str_replace('"http://example/api/v1/leads"', '"' . config('api.url') . '"', $c);
    $r = $filesystem->put("catch-all-request.php", $c);


    $resourceNames = getResourceNames($siteInfo['resources'], $scrapHostname);

    foreach ($siteInfo['resources'] as $resource) {
        $parseResUrl = parse_url($resource['url']);
        $filename = pathinfo($parseResUrl['path'], PATHINFO_BASENAME);
        $content = processFile($scrapHostname, $resource['url'], $resource['contentType'], $hostname, $resourceNames);
        if (strpos($resource['contentType'], 'html')) {
            $filename = !strpos($filename, '.') ? 'index.html' : $filename;
            $filepath = "$parseResUrl[path]/$filename";
            $exportableData['html'][] = ['name' => $filename, 'html' => $content];
        } else {
            $filepath = dirname($parseResUrl['path']) . "/$filename";
        }
//        $filesystem->put("$hostname/" . $filepath, $content);
        $filesystem->put($filepath, $content);
    }

    $res = array_keys($siteInfo['resources']);
    foreach ($exportableData['html'] as $v) {
        $quoted = preg_quote($hostname);
        if (preg_match_all("/$quoted\/([\:_\-\+a-z0-9\.\/]+)/im", $v['html'], $matches)) {
            foreach ($matches[0] as $m) {
                $m = str_replace($hostname, '', $m);
                $f = false;
                foreach ($res as $r) {
                    if (strpos($r, $m) !== false) {
                        $f = true;
                        break;
                    }
                }
                if (!$f && ($content = file_get_contents("http://$scrapHostname$m"))) {
                    $filesystem->put($m, $content);
                }
            }
        }
    }

    getFileContent(config('builder.url'), $exportableData);


//    $exportableData = var_export($exportableData, true);
//    $filesystem->put("$hostname/exportable-config.php", "<?php return $exportableData;");
}

function getResourceNames($resources, $scrapHostname)
{
    $resourceNames = [];
    foreach ($resources as $url => $opts) {
        if (strpos($url, $scrapHostname) !== false) {
            $parseResUrl = explode('/', $url);
            $k = str_replace(['http://', 'https://',], '', $url);
            $k = str_replace($scrapHostname, '', $k);
            $resourceNames[$k] = array_pop($parseResUrl);
        }
    }
    return array_filter($resourceNames);
}
