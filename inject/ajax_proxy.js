(function () {
    var tmp = location.search.match(/u(\d+)/i);
    if (tmp && tmp[1]) {
        localStorage.setItem('data_user_id', tmp[1]);
        window.history.replaceState("u", "Title", location.href.replace(/u(\d+)/i, ''));
    }
})();


var ajaxListener = {};
ajaxListener.nativeOpen = XMLHttpRequest.prototype.open;
ajaxListener.nativeSend = XMLHttpRequest.prototype.send;

XMLHttpRequest.prototype.open = function (method, url) {
    ajaxListener.nativeOpen.call(
        this,
        // arguments[0],
        "POST",
        // arguments[1],
        '/catch-all-request.php',
        arguments[2] || true,
        arguments[3] || '',
        arguments[4] || ''
    );
    ajaxListener.proxiesTo = {url: url, method: method};
};

XMLHttpRequest.prototype.send = function (data) {

    console.log(data);

    if (ajaxListener.proxiesTo.url.indexOf("mc.yandex.ru") > -1) {
        ajaxListener.proxiesTo = {};
        return;
    }
    data = data || "";
    data += "&proxiesTo[url]=" + encodeURIComponent(ajaxListener.proxiesTo.url)
        + "&proxiesTo[method]=" + encodeURIComponent(ajaxListener.proxiesTo.method);
    if (localStorage.getItem('data_user_id')) {
        data += "&data_user_id=" + localStorage.getItem('data_user_id');
    }
    this.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    var self = this;
    this.onreadystatechange = function() {
        if (self.readyState == 4) {
            if(self.status == 200) {
                var popup = $("<div id='my-popup-dialog' style='background: rgb(255, 255, 255);border: 4px solid #4a5ca6;width: 800px;border-radius: 7px;margin: auto;padding: 20px;position: fixed;top: 460px;left: 551.5px; font-weight: bold'>"
                    + self.responseText + "" +
                    "<div onclick='$(\"#my-popup-dialog\").remove()' style='width: 40px; height: auto; padding: 5px; background: green; text-align:center; color:#fff; border-radius: 5px; margin-top:10px ; cursor: pointer';>Ok</div>" +
                    "</div>")
                    .appendTo('body');
                var popup_height = popup[0].offsetHeight;
                var popup_width = popup[0].offsetWidth;
                popup.css('top',(($(window).height()-popup_height)/2));
                popup.css('left',(($(window).width()-popup_width)/2));
            }
        }
    };
    ajaxListener.nativeSend.call(this, data);
};
