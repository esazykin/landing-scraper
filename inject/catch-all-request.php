<?php

$taskId = 0; //!

if (!empty($_POST['proxiesTo']) && !empty($_POST['proxiesTo']['url']) && !empty($_POST['proxiesTo']['method'])) {
    $proxied = $_POST['proxiesTo'];
    $userId = isset($_POST['data_user_id']) ? $_POST['data_user_id'] : 0;
    $filepath = realpath("./$proxied[url]");
    if (file_exists($filepath) && strtolower($proxied['method']) == 'get') {
        echo file_get_contents($filepath);
        exit;
    }

    unset($_POST['proxiesTo'], $_POST['data_user_id']);
    $data = [
        'headers' => [
            'user_agent' => $_SERVER['HTTP_USER_AGENT'],
            'server_name' => $_SERVER['SERVER_NAME'],
            'remote_addr' => $_SERVER['REMOTE_ADDR'],
        ],
        'proxy' => $proxied,
        'data' => $_POST,
        'data_user_id' => $userId,
        'task_domain_id' => $taskId
    ];

//    print_r($data);exit;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://example/api/v1/leads");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec($ch);
    curl_close($ch);

//    $output = '{"result":"ok","original_domain":"http://shot-mails.ru/", "original_fields":["project_name", "form_subject", "mess", "skype", "phone", "email", "name", "submit"]}';
    $output = json_decode($output, true);
    if ($output['result'] == 'ok') {
        $ch = curl_init();
        $data = [];
        foreach ($_POST as $k => $v) {
            if (in_array($k, $output['original_fields'])) {
                $data[$k] = $v;
            }
        }
        $data = http_build_query($data);
        $url = rtrim($output['original_domain'], '/') . '/' . ltrim($proxied['url'], '/');
        if (strtolower($proxied['method']) == 'get') {
            $url = trim($url, '?') . "?$data";
        } else {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }

        //echo '<pre>';        var_dump($url, $data);exit;


        curl_setopt($ch, CURLOPT_REFERER, $output['original_domain']);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/53.0.2785.143 Chrome/53.0.2785.143 Safari/537.36');
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);

//        var_dump($output);
        echo 'Спасибо! Ваш запрос будет обработан в ближайшее время.';
    }
}
