<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
include 'vendor/autoload.php';

iniErrorHandler();

$tasksDomainId = $argv[1];
$address = $argv[2];
$newDomain = $argv[3];
$ispConn = $argv[4];

if (!isset($address, $newDomain)) {
    errorResult('You have insufficient information', 1);
}
$parsedAddress = parse_url($address);
if (!filter_var(gethostbyname($parsedAddress['host']), FILTER_VALIDATE_IP)) {
    errorResult("The domain '$address' isn't valid", 10);
}

$siteInfo = getSiteInfo($address);
if (empty($siteInfo['resources'])) {
    errorResult("None of the resources was not loaded. I made a request to url $siteInfo[url]", 20);
}

$hostname = parse_url($newDomain)['host'];

$isp = db()::table('isp_servers')->find($ispConn);
$m = ISPManager([$isp->ip_address, $isp->port, $isp->login, $isp->password]);
$domainInfo = ($m->createDomain)($hostname, "admin@$hostname");
if (isset($domainInfo['error'])) {
    errorResult(json_encode($domainInfo['error']), 2);
}

uploadFiles($parsedAddress['host'], $hostname, $siteInfo, $isp, $tasksDomainId);

successResult($domainInfo);
