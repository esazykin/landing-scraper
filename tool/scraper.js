
/**
 * Set up page and script parameters
 */
var page = require('webpage').create(),
    system = require('system'),
    libUrl = require('phantom-url'),
    response = {},
    debug = [],
    logs = [],
    procedure = {},
    resources = 0,
    resourcesLinks = {},
    timeout,
    address = system.args[1],
    hostname = libUrl(address).hostname;


/**
 * Define width & height of capture
 */
var rectTop = 0,
    rectLeft = 0,
    rectWidth = 0,
    rectHeight = 0;

if (rectWidth && rectHeight) {
    debug.push(new Date().toISOString().slice(0, -5) + ' [INFO] PhantomJS - Set capture clipping size ~ top: ' + rectTop + ' left: ' + rectLeft + ' ' + rectWidth + 'x' + rectHeight);
    page.clipRect = {
        top: rectTop,
        left: rectLeft,
        width: rectWidth,
        height: rectHeight
    };
}


/**
 * Define paper size.
 */


/**
 * Define viewport size.
 */

var viewportWidth = 1100,
    viewportHeight = 1000;

if (viewportWidth && viewportHeight) {

    debug.push(new Date().toISOString().slice(0, -5) + ' [INFO] PhantomJS - Set viewport size ~ width: ' + viewportWidth + ' height: ' + viewportHeight);

    page.viewportSize = {
        width: viewportWidth,
        height: viewportHeight
    };
}


/**
 * Define custom headers.
 */

page.customHeaders = {};


/**
 * Page settings
 */

page.settings.resourceTimeout = 100000;


/**
 * On resource timeout
 */
page.onResourceTimeout = function (error) {
    response = error;
    response.status = error.errorCode;
};

/**
 * On resource requested
 */
page.onResourceRequested = function (req) {
    resources++;
    window.clearTimeout(timeout);
    // console.log(req.url);
};

/**
 * On resource received
 */
page.onResourceReceived = function (res) {
    if (!response.status) {
        response = res;
    }

    if (!res.stage || res.stage === 'end') {
        resources--;
        if (libUrl(res.url).hostname == hostname && !(res.url in resourcesLinks)) {
            resourcesLinks[res.url] = res;
        }

        if (resources === 0) {
            timeout = window.setTimeout(function () {
                procedure.execute('success');
            }, 300);
        }
    }
};

/**
 * Handle page errors
 */
page.onError = function (msg, trace) {

    var error = {
        message: msg,
        trace: []
    };

    trace.forEach(function (t) {
        error.trace.push(' -> ' + (t.file || t.sourceURL) + ': ' + t.line + (t.function ? ' (in function ' + t.function + ')' : ''));
    });

    logs.push(error);
};

/**
 * Handle global errors
 */
phantom.onError = function (msg, trace) {

    var stack = [];

    trace.forEach(function (t) {
        stack.push(' -> ' + (t.file || t.sourceURL) + ': ' + t.line + (t.function ? ' (in function ' + t.function + ')' : ''));
    });

    response.status = 500;
    response.content = msg;
    response.console = stack;

    system.stdout.write(JSON.stringify(response, undefined, 4));
    phantom.exit(1);


};

/**
 * Open page
 */
page.open(address, 'GET', '', function (status) {
    // page.evaluate(function () {
    //     var styles = {};styles) {
    //         document.body.style[property] = styles[property];
    //     }
    // });

    window.setTimeout(function () {
        procedure.execute(status);
    }, 48000);
});

/**
 * Execute procedure
 */
procedure.execute = function (status) {
    if (status === 'success') {
        try {
            response.screen = page.renderBase64('JPEG');
            // page.render('./scr.jpg', {
            //     format: 'jpeg',
            //     quality: 75,
            // });

            // response.content = page.evaluate(function () {
            //     return document.getElementsByTagName('html')[0].innerHTML
            // });
            // logs.push('received: ' + JSON.stringify(resourcesLinks, undefined, 4));
            response.resources = resourcesLinks;
        } catch (e) {
            response.status = 500;
            response.content = e.message;
        }
    }

    response.console = logs;

    system.stderr.write(debug.join('\\n') + '\\n');
    system.stdout.write(JSON.stringify(response));

    phantom.exit();
};
