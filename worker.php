<?php

echo "Starting GearmanWorker\n";

$conf = (require_once __DIR__ . "/config/config.php")['gearman'];

$gmworker = new GearmanWorker();
$gmworker->addServer($conf['host'], $conf['port']);
$gmworker->addFunction($conf['functions']['scrape'], function ($job) {
    $data = unserialize($job->workload());
    $params = $data['params'];
    if (!isset($params['task_domain_id'], $params['in_domain'], $params['out_domain'], $params['isp_id'])) {
        return json_encode(['error' => 1]);
    }
    $cmd = escapeshellcmd("php ./scraper.php $params[task_domain_id] $params[in_domain] $params[out_domain] $params[isp_id]");
    return system($cmd);
});

while ($gmworker->work()) {
    if ($gmworker->returnCode() != GEARMAN_SUCCESS) {
        echo "return_code: " . $gmworker->returnCode() . "\n";
        break;
    }
}